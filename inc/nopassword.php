<?php
namespace nopassword;
/*
 * getpdo($localdbusername, $options=array())
 *
 * returns PDO object connected as $username to mysql running on localhost
 * with PDO options array $options
 *
 * example usage:
 * $pdo = getpdo('webdb', 'dbuserAPI1', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_PERSISTENT => true));
 * 
 */
function getpdo($dbname, $localdbusername, $options = array()) {
  try {
    // find (secret) database password (loading all global passwords into a global variable)
    require(dirname(__FILE__) . '/../serverconfig/secrets/nopassword-dbpass.php');
    $usr = $localdbusername . '@localhost';
    $pass = '';
    if (isset($nopassword_dbpass[$usr])) {
      $pass = $nopassword_dbpass[$usr];
    }
    // forget database passwords
    unset($config_dbpass);
  } catch (\Exception $e) {
    $pass = '';
  }
  if ($pass == '') {
    throw new \Exception("nopassword.php could not find the password for database user $localdbusername. Check configuration, directory structure, and that you 
have generated local passwords using nopassword-setdbpass.sh");
  }
  try {
    return new \PDO('mysql:host=localhost;dbname='.$dbname.';charset=utf8', $localdbusername, $pass, $options);
  } catch (\PDOException $pe) {
    throw new \PDOException("nopassword.php could not open a PDO connection for databse user $localdbusername");
  }
}

/*
 * findpassword($name, $service='')
 *
 * lookup non-database passwords from ../serverconfig/secrets/nopassword-pass.php
 *
 * example usage:
 * $pw = findpassword('APIkey', 'website');
 * 
 */
function findpassword($name, $service='') {
  try {
    require(dirname(__FILE__) . '/../serverconfig/secrets/nopassword-pass.php');
    if ($service !== '') {
      $name = $name . '@' . $service;
    }
    $pass = '';
    if (isset($nopassword_pass[$name])) {
      $pass = $nopassword_pass[$name];
    }
    // forget all other passwords
    unset($config_pass);
  } catch (\Exception $e) {
    $pass = '';
  }
  if ($pass == '') {
    throw new \Exception("nopassword.php could not find the password for $name. Check that you have entered the password in ../serverconfig/secrets/nopassword-pass.php");
  }
  return $pass;
}

?>