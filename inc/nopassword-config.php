<?php 
namespace nopassword;

// configuration file for nopassword,
// bitbucket.org/steinb/nopassword/src

// database name
$dbname = "testdb";

// list of database users that setdbpass.php shall generate passwords for
// (database users for automated use by php scripts)

$config_authuser = array (
  'dbuserAPI1@localhost',
  'dbuserAPI2@localhost'
); 

?>