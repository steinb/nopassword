#!/bin/bash

(

# try to change the current directory to where this file (and other required files) are located
cd `dirname $0`

echo "Executing nopassword-setdbpass.php and piping the result into mysql -u root -p..."
echo "This should set new random database passwords for all db users listed in nopassword-config.php."
echo "You will need to manually enter the password for the mysql root user."

php nopassword-setdbpass.php  |mysql -u root -p

)
